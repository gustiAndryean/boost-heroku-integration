const Sequelize = require('sequelize');
const db = require('../configs/database');

const User = db.define('users', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  firstname: {
    type: Sequelize.STRING,
    allowNull: false
  },
  gender: {
    type: Sequelize.STRING,
    allowNull: false
  }
}, {
  tableName: 'users',
  timestamps: true
});

User.belongsTo

module.exports = User;
