const elasticsearch = require('elasticsearch');
const config = require('../config');

const { host, auth, protocol, port } = config.get('ELK');
const ELK = new elasticsearch.Client({
  host: [
    {
      host,
      auth,
      protocol,
      port
    }
  ]
});

module.exports = ELK;
