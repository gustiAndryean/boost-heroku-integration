const winston = require('winston');
require('winston-daily-rotate-file');

const config = require('./../config');
const fs = require('fs');

const logDir = `${config.get('LOG_DIR')}/`;
const ELK = require('./ELK');
const serviceName = config.get('SERVICE_NAME');

if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

const transport = new (winston.transports.DailyRotateFile)({
  filename: '%DATE%.log',
  dirname: logDir,
  datePattern: 'YYYY-MM-DD-HH',
  maxSize: '20m'
});

const winstonLogger = new (winston.Logger)({
  transports: [
    transport
  ]
});

const logger = {
  info: (message) => {
    winstonLogger.info(message);

    ELK.index({
      index: `${serviceName}-${new Date()}`,
      type: '_doc',
      body: {
        type: 'info',
        serviceName,
        message
      }
    });
  },
  error: (error) => {
    winstonLogger.error(error);

    ELK.index({
      index: `${serviceName}-${new Date()}`,
      type: '_doc',
      body: {
        type: 'error',
        serviceName,
        error
      }
    });
  }
};

module.exports = logger;