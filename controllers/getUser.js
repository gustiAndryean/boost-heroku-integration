const User = require('../models/user');
const logger = require('../libs/logger');

const getUser =  async (req, res) => {
  logger.info(`[${req.requestId}] HIT GETUSER ENDPOINT`);
  const where = { id: req.params.id }
  let result;

  try {
    logger.info(`[${req.requestId}] REQUEST  - GET USER BY ID : ${JSON.stringify(req.params.id, null, 2)}`);
    result = await User.findAll({ where });
    logger.info(`[${req.requestId}] RESPONSE  - GET USER BY ID : ${JSON.stringify(result, null, 2)}`);
  } catch (error) {
    logger.error(`[${req.requestId}] ERROR  - GET USER BY ID : ${JSON.stringify(error, null, 2)}`);
    return res.status(500).json({
      result: error
    })
  }
  logger.info(`[${req.requestId}] RETURN  - GET USER BY ID : ${JSON.stringify(result, null, 2)}`);
  return res.status(200).json({
    result
  })
}

module.exports = (router) => {
  router.get('/:id', getUser);
}