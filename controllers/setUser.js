const User = require('../models/user');
const logger = require('../libs/logger');

const setUser =  async (req, res) => {
  logger.info(`[${req.requestId}] HIT CREATE USER ENDPOINT`);
  let result;

  try {
    logger.info(`[${req.requestId}] REQUEST  - CREATE USER : ${JSON.stringify(req.params, null, 2)}`);
    result = await User.create({
      firstname: req.params.name,
      gender: req.params.gender
    });
    logger.info(`[${req.requestId}] RESPONSE  - CREATE USER : ${JSON.stringify(result, null, 2)}`);
  } catch (error) {
    logger.error(`[${req.requestId}] ERROR  - CREATE USER : ${JSON.stringify(error, null, 2)}`);
    return res.status(500).json({
      error
    })
  }
  logger.info(`[${req.requestId}] RETURN  - CREATE USER : ${JSON.stringify(result, null, 2)}`);
  return res.status(200).json({
    result
  })
}

module.exports = (router) => {
  router.get('/:name/:gender', setUser);
}