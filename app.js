require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const enrouten = require('express-enrouten');
const path = require('path');

const app = express();
const requestId = require('./libs/requestId');
const { PORT } = process.env;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(requestId());
app.use(enrouten({
  directory: path.join(__dirname, 'controllers'),
}));

app.listen(PORT, () => {
  console.log(`service running on port ${PORT}`)
});